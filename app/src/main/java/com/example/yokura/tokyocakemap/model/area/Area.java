package com.example.yokura.tokyocakemap.model.area;

/**
 * Created by YOKURA on 6/4/15.
 */

import org.json.*;
import io.realm.*;
import io.realm.annotations.*;


@RealmClass
public class Area extends RealmObject {

    private String areaCode;
    private String areaName;

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return this.areaCode;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaName() {
        return this.areaName;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static Area fromJson(Realm realm, JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        Area area = realm.createObject(Area.class);
        area.areaCode = jsonObject.optString("area_code");
        area.areaName = jsonObject.optString("area_name");
        return area;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(Area area) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("area_code", area.areaCode);
            jsonObject.put("area_name", area.areaName);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }
}
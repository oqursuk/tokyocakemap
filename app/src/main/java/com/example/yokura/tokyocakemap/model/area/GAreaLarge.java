package com.example.yokura.tokyocakemap.model.area;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;


@RealmClass
public class GAreaLarge extends RealmObject{

    private String areacodeL;
    private String areanameL;
    private Pref pref;

    public void setAreacodeL(String areacodeL){
        this.areacodeL = areacodeL;
    }
    public String getAreacodeL(){
        return this.areacodeL;
    }
    public void setAreanameL(String areanameL){
        this.areanameL = areanameL;
    }
    public String getAreanameL(){
        return this.areanameL;
    }
    public void setPref(Pref pref){
        this.pref = pref;
    }
    public Pref getPref(){
        return this.pref;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static GAreaLarge fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        GAreaLarge GAreaLarge = realm.createObject(GAreaLarge.class);
        GAreaLarge.areacodeL = jsonObject.optString("areacode_l");
        GAreaLarge.areanameL = jsonObject.optString("areaname_l");
        GAreaLarge.pref = Pref.fromJson(realm, jsonObject.optJSONObject("pref"));
        return GAreaLarge;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(GAreaLarge GAreaLarge)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areacode_l", GAreaLarge.areacodeL);
            jsonObject.put("areaname_l", GAreaLarge.areanameL);
            jsonObject.put("pref", Pref.toJsonObject(GAreaLarge.pref));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
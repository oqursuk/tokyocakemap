package com.example.yokura.tokyocakemap.model.area;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;


@RealmClass
public class GAreaMiddle extends RealmObject{

    private String areacodeM;
    private String areanameM;
    private GAreaLarge gAreaLarge;
    private Pref pref;

    public void setAreacodeM(String areacodeM){
        this.areacodeM = areacodeM;
    }
    public String getAreacodeM(){
        return this.areacodeM;
    }
    public void setAreanameM(String areanameM){
        this.areanameM = areanameM;
    }
    public String getAreanameM(){
        return this.areanameM;
    }
    public void setGAreaLarge(GAreaLarge GAreaLarge){
        this.gAreaLarge = GAreaLarge;
    }
    public GAreaLarge getGAreaLarge(){
        return this.gAreaLarge;
    }
    public void setPref(Pref pref){
        this.pref = pref;
    }
    public Pref getPref(){
        return this.pref;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static GAreaMiddle fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        GAreaMiddle GAreaMiddle = realm.createObject(GAreaMiddle.class);
        GAreaMiddle.areacodeM = jsonObject.optString("areacode_m");
        GAreaMiddle.areanameM = jsonObject.optString("areaname_m");
        GAreaMiddle.gAreaLarge = GAreaLarge.fromJson(realm, jsonObject.optJSONObject("garea_large"));
        GAreaMiddle.pref = Pref.fromJson(realm, jsonObject.optJSONObject("pref"));
        return GAreaMiddle;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(GAreaMiddle GAreaMiddle)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areacode_m", GAreaMiddle.areacodeM);
            jsonObject.put("areaname_m", GAreaMiddle.areanameM);
            jsonObject.put("garea_large", GAreaLarge.toJsonObject(GAreaMiddle.gAreaLarge));
            jsonObject.put("pref", Pref.toJsonObject(GAreaMiddle.pref));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
package com.example.yokura.tokyocakemap.model.area;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;


@RealmClass
public class GAreaSmall extends RealmObject{

    private String areacodeS;
    private String areanameS;
    private GAreaLarge gAreaLarge;
    private GAreaMiddle gAreaMiddle;
    private Pref pref;

    public void setAreacodeS(String areacodeS){
        this.areacodeS = areacodeS;
    }
    public String getAreacodeS(){
        return this.areacodeS;
    }
    public void setAreanameS(String areanameS){
        this.areanameS = areanameS;
    }
    public String getAreanameS(){
        return this.areanameS;
    }
    public void setGAreaLarge(GAreaLarge GAreaLarge){
        this.gAreaLarge = GAreaLarge;
    }
    public GAreaLarge getGAreaLarge(){
        return this.gAreaLarge;
    }
    public void setGAreaMiddle(GAreaMiddle GAreaMiddle){
        this.gAreaMiddle = GAreaMiddle;
    }
    public GAreaMiddle getGAreaMiddle(){
        return this.gAreaMiddle;
    }
    public void setPref(Pref pref){
        this.pref = pref;
    }
    public Pref getPref(){
        return this.pref;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static GAreaSmall fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        GAreaSmall GAreaSmall = realm.createObject(GAreaSmall.class);
        GAreaSmall.areacodeS = jsonObject.optString("areacode_s");
        GAreaSmall.areanameS = jsonObject.optString("areaname_s");
        GAreaSmall.gAreaLarge = GAreaLarge.fromJson(realm, jsonObject.optJSONObject("garea_large"));
        GAreaSmall.gAreaMiddle = GAreaMiddle.fromJson(realm, jsonObject.optJSONObject("garea_middle"));
        GAreaSmall.pref = Pref.fromJson(realm, jsonObject.optJSONObject("pref"));
        return GAreaSmall;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(GAreaSmall GAreaSmall)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areacode_s", GAreaSmall.areacodeS);
            jsonObject.put("areaname_s", GAreaSmall.areanameS);
            jsonObject.put("garea_large", GAreaLarge.toJsonObject(GAreaSmall.gAreaLarge));
            jsonObject.put("garea_middle", GAreaMiddle.toJsonObject(GAreaSmall.gAreaMiddle));
            jsonObject.put("pref", Pref.toJsonObject(GAreaSmall.pref));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
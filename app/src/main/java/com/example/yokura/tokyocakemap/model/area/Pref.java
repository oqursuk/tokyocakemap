package com.example.yokura.tokyocakemap.model.area;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by YOKURA on 5/7/15.
 */


@RealmClass
public class Pref extends RealmObject {

    private String areaCode;
    private String prefCode;
    private String prefName;

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return this.areaCode;
    }

    public void setPrefCode(String prefCode) {
        this.prefCode = prefCode;
    }

    public String getPrefCode() {
        return this.prefCode;
    }

    public void setPrefName(String prefName) {
        this.prefName = prefName;
    }

    public String getPrefName() {
        return this.prefName;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static Pref fromJson(Realm realm, JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        Pref pref = realm.createObject(Pref.class);
        pref.areaCode = jsonObject.optString("area_code");
        pref.prefCode = jsonObject.optString("pref_code");
        pref.prefName = jsonObject.optString("pref_name");
        return pref;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(Pref pref) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("area_code", pref.areaCode);
            jsonObject.put("pref_code", pref.prefCode);
            jsonObject.put("pref_name", pref.prefName);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
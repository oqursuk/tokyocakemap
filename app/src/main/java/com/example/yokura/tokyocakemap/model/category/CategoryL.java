package com.example.yokura.tokyocakemap.model.category;

import org.json.*;
import io.realm.*;
import io.realm.annotations.*;


@RealmClass
public class CategoryL extends RealmObject{

    private String categoryLCode;
    private String categoryLName;

    public void setCategoryLCode(String categoryLCode){
        this.categoryLCode = categoryLCode;
    }
    public String getCategoryLCode(){
        return this.categoryLCode;
    }
    public void setCategoryLName(String categoryLName){
        this.categoryLName = categoryLName;
    }
    public String getCategoryLName(){
        return this.categoryLName;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static CategoryL fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        CategoryL categoryL = realm.createObject(CategoryL.class);
        categoryL.categoryLCode = jsonObject.optString("category_l_code");
        categoryL.categoryLName = jsonObject.optString("category_l_name");
        return categoryL;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(CategoryL categoryL)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category_l_code", categoryL.categoryLCode);
            jsonObject.put("category_l_name", categoryL.categoryLName);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}

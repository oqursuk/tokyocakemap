package com.example.yokura.tokyocakemap.model.category;

import org.json.*;
import io.realm.*;
import io.realm.annotations.*;

@RealmClass
public class CategoryS extends RealmObject{
    private String categoryLCode;
    private String categorySCode;
    private String categorySName;

    public void setCategoryLCode(String categoryLCode) {
        this.categoryLCode = categoryLCode;
    }

    public String getCategoryLCode() {
        return this.categoryLCode;
    }

    public void setCategorySCode(String categorySCode) {
        this.categorySCode = categorySCode;
    }

    public String getCategorySCode() {
        return this.categorySCode;
    }

    public void setCategorySName(String categorySName) {
        this.categorySName = categorySName;
    }

    public String getCategorySName() {
        return this.categorySName;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static CategoryS fromJson(Realm realm, JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        CategoryS categoryS = realm.createObject(CategoryS.class);
        categoryS.categoryLCode = jsonObject.optString("category_l_code");
        categoryS.categorySCode = jsonObject.optString("category_s_code");
        categoryS.categorySName = jsonObject.optString("category_s_name");
        return categoryS;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(CategoryS categoryS) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category_l_code", categoryS.categoryLCode);
            jsonObject.put("category_s_code", categoryS.categorySCode);
            jsonObject.put("category_s_name", categoryS.categorySName);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }
}


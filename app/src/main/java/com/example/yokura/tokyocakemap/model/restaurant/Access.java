package com.example.yokura.tokyocakemap.model.restaurant;//
//	Acces.java
//
//	Create by 佑介 大倉 on 4/6/2015
//	Copyright © 2015. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.*;
import io.realm.*;
import io.realm.annotations.*;


@RealmClass
public class Access extends RealmObject{

    private String line;
    private String note;
    private String station;
    private String stationExit;
    private int walk;

    public void setLine(String line){
        this.line = line;
    }
    public String getLine(){
        return this.line;
    }
    public void setNote(String note){
        this.note = note;
    }
    public String getNote(){
        return this.note;
    }
    public void setStation(String station){
        this.station = station;
    }
    public String getStation(){
        return this.station;
    }
    public void setStationExit(String stationExit){
        this.stationExit = stationExit;
    }
    public String getStationExit(){
        return this.stationExit;
    }
    public void setWalk(int walk){
        this.walk = walk;
    }
    public int getWalk(){
        return this.walk;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static Access fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        Access acces = realm.createObject(Access.class);
        acces.line =  jsonObject.optString("line");
        acces.note =  jsonObject.optString("note");
        acces.station =  jsonObject.optString("station");
        acces.stationExit =  jsonObject.optString("station_exit");
        acces.walk = (int) jsonObject.opt("walk");
        return acces;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(Access acces)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("line", acces.line);
            jsonObject.put("note", acces.note);
            jsonObject.put("station", acces.station);
            jsonObject.put("station_exit", acces.stationExit);
            jsonObject.put("walk", acces.walk);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
package com.example.yokura.tokyocakemap.model.restaurant;

import com.example.yokura.tokyocakemap.model.category.CategoryL;
import com.example.yokura.tokyocakemap.model.category.CategoryS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by YOKURA on 6/4/15.
 */
@RealmClass
public class Code extends RealmObject {
//
    private String areacode;
//    private String areaname;
//    private String areanameS;
////    private RealmList<String> categoryCodeL;
////    private RealmList<String> categoryCodeS;
////    private RealmList<String> categoryNameL;
////    private RealmList<String> categoryNameS;
//    private RealmList<CategoryL> categoryCodeL;
////    private RealmList<String> categoryNameL;
//    private RealmList<CategoryS> categoryCodeS;
////    private RealmList<String> categoryNameS;
//    private String prefcode;
//    private String prefname;
//
    public void setAreacode(String areacode){
        this.areacode = areacode;
    }
    public String getAreacode(){
        return this.areacode;
    }
//    public void setAreaname(String areaname){
//        this.areaname = areaname;
//    }
//    public String getAreaname(){
//        return this.areaname;
//    }
//    public void setAreanameS(String areanameS){
//        this.areanameS = areanameS;
//    }
//    public String getAreanameS(){
//        return this.areanameS;
//    }
//    public void setCategoryCodeL(RealmList<String> categoryCodeL){
//        this.categoryCodeL = categoryCodeL;
//    }
//    public RealmList<String> getCategoryCodeL(){
//        return this.categoryCodeL;
//    }
//    public void setCategoryCodeS(RealmList<String> categoryCodeS){
//        this.categoryCodeS = categoryCodeS;
//    }
//    public RealmList<String> getCategoryCodeS(){
//        return this.categoryCodeS;
//    }
//    public void setCategoryNameL(RealmList<String> categoryNameL){
//        this.categoryNameL = categoryNameL;
//    }
//    public RealmList<String> getCategoryNameL(){
//        return this.categoryNameL;
//    }
//    public void setCategoryNameS(RealmList<String> categoryNameS){
//        this.categoryNameS = categoryNameS;
//    }
//    public RealmList<String> getCategoryNameS(){
//        return this.categoryNameS;
//    }
//    public void setPrefcode(String prefcode){
//        this.prefcode = prefcode;
//    }
//    public String getPrefcode(){
//        return this.prefcode;
//    }
//    public void setPrefname(String prefname){
//        this.prefname = prefname;
//    }
//    public String getPrefname(){
//        return this.prefname;
//    }
//
    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static Code fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }

        Code code = realm.createObject(Code.class);
//        code.areacode = jsonObject.optString("areacode");
//        code.areaname = jsonObject.optString("areaname");
//        code.areanameS = jsonObject.optString("areaname_s");
//        JSONArray categoryCodeLTmp = jsonObject.optJSONArray("category_code_l");
//
//        if(categoryCodeLTmp != null){
//            categoryCodeL = new String[categoryCodeLTmp.length()];
//            for(int i = 0; i < categoryCodeLTmp.length(); i++){
//                categoryCodeL[i] = categoryCodeLTmp.get(i);
//            }
//        }
//        JSONArray categoryCodeSTmp = jsonObject.optJSONArray("category_code_s");
//        if(categoryCodeSTmp != null){
//            categoryCodeS = new String[categoryCodeSTmp.length()];
//            for(int i = 0; i < categoryCodeSTmp.length(); i++){
//                categoryCodeS[i] = categoryCodeSTmp.get(i);
//            }
//        }
//        JSONArray categoryNameLTmp = jsonObject.optJSONArray("category_name_l");
//        if(categoryNameLTmp != null){
//            categoryNameL = new String[categoryNameLTmp.length()];
//            for(int i = 0; i < categoryNameLTmp.length(); i++){
//                categoryNameL[i] = categoryNameLTmp.get(i);
//            }
//        }
//        JSONArray categoryNameSTmp = jsonObject.optJSONArray("category_name_s");
//        if(categoryNameSTmp != null){
//            categoryNameS = new String[categoryNameSTmp.length()];
//            for(int i = 0; i < categoryNameSTmp.length(); i++){
//                categoryNameS[i] = categoryNameSTmp.get(i);
//            }
//        }
//        code.prefcode = jsonObject.opt("prefcode");
//        code.prefname = jsonObject.opt("prefname");
        return code;
    }
//
//    /**
//     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
//     */
    public static JSONObject toJsonObject(Code code)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areacode", code.areacode);
//            jsonObject.put("areaname", code.areaname);
//            jsonObject.put("areaname_s", code.areanameS);
//            jsonObject.put("category_code_l", code.categoryCodeL);
//            jsonObject.put("category_code_s", code.categoryCodeS);
//            jsonObject.put("category_name_l", code.categoryNameL);
//            jsonObject.put("category_name_s", code.categoryNameS);
//            jsonObject.put("prefcode", code.prefcode);
//            jsonObject.put("prefname", code.prefname);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}



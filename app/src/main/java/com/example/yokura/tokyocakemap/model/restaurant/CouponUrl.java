package com.example.yokura.tokyocakemap.model.restaurant;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by YOKURA on 6/4/15.
 */
@RealmClass
public class CouponUrl extends RealmObject {

    private String mobile;
    private String pc;

    public void setMobile(String mobile){
        this.mobile = mobile;
    }
    public String getMobile(){
        return this.mobile;
    }
    public void setPc(String pc){
        this.pc = pc;
    }
    public String getPc(){
        return this.pc;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static CouponUrl fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        CouponUrl couponUrl = realm.createObject(CouponUrl.class);
        couponUrl.mobile = (String) jsonObject.opt("mobile");
        couponUrl.pc = (String) jsonObject.opt("pc");
        return couponUrl;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(CouponUrl couponUrl)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", couponUrl.mobile);
            jsonObject.put("pc", couponUrl.pc);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}


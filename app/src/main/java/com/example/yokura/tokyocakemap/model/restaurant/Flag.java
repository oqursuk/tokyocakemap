package com.example.yokura.tokyocakemap.model.restaurant;

/**
 * Created by YOKURA on 6/4/15.
 */

import org.json.*;
import io.realm.*;
import io.realm.annotations.*;


@RealmClass
public class Flag extends RealmObject{

    private int mobileCoupon;
    private int mobileSite;
    private int pcCoupon;

    public void setMobileCoupon(int mobileCoupon){
        this.mobileCoupon = mobileCoupon;
    }
    public int getMobileCoupon(){
        return this.mobileCoupon;
    }
    public void setMobileSite(int mobileSite){
        this.mobileSite = mobileSite;
    }
    public int getMobileSite(){
        return this.mobileSite;
    }
    public void setPcCoupon(int pcCoupon){
        this.pcCoupon = pcCoupon;
    }
    public int getPcCoupon(){
        return this.pcCoupon;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static Flag fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        Flag flag = realm.createObject(Flag.class);
        flag.mobileCoupon = jsonObject.optInt("mobile_coupon");
        flag.mobileSite = jsonObject.optInt("mobile_site");
        flag.pcCoupon = jsonObject.optInt("pc_coupon");
        return flag;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(Flag flag)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile_coupon", flag.mobileCoupon);
            jsonObject.put("mobile_site", flag.mobileSite);
            jsonObject.put("pc_coupon", flag.pcCoupon);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
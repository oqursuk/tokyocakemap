package com.example.yokura.tokyocakemap.model.restaurant;

/**
 * Created by YOKURA on 6/4/15.
 */

import org.json.*;
import io.realm.*;
import io.realm.annotations.*;


@RealmClass
public class ImageUrl extends RealmObject{

    private String qrcode;
    private String shopImage1;
    private String shopImage2;

    public void setQrcode(String qrcode){
        this.qrcode = qrcode;
    }
    public String getQrcode(){
        return this.qrcode;
    }
    public void setShopImage1(String shopImage1){
        this.shopImage1 = shopImage1;
    }
    public String getShopImage1(){
        return this.shopImage1;
    }
    public void setShopImage2(String shopImage2){
        this.shopImage2 = shopImage2;
    }
    public String getShopImage2(){
        return this.shopImage2;
    }

    /**
     * Creates instance using the passed realm and jsonObject to set the properties values
     */
    public static ImageUrl fromJson(Realm realm, JSONObject jsonObject){
        if(jsonObject == null){
            return null;
        }
        ImageUrl imageUrl = realm.createObject(ImageUrl.class);
        imageUrl.qrcode =  jsonObject.optString("qrcode");
        imageUrl.shopImage1 =  jsonObject.optString("shop_image1");
        imageUrl.shopImage2 =  jsonObject.optString("shop_image2");
        return imageUrl;
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public static JSONObject toJsonObject(ImageUrl imageUrl)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("qrcode", imageUrl.qrcode);
            jsonObject.put("shop_image1", imageUrl.shopImage1);
            jsonObject.put("shop_image2", imageUrl.shopImage2);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
package com.example.yokura.tokyocakemap.model.restaurant;

/**
 * Created by YOKURA on 6/4/15.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pr {

    @SerializedName("pr_short")
    @Expose
    private String prShort;
    @SerializedName("pr_long")
    @Expose
    private String prLong;

    /**
     *
     * @return
     * The prShort
     */
    public String getPrShort() {
        return prShort;
    }

    /**
     *
     * @param prShort
     * The pr_short
     */
    public void setPrShort(String prShort) {
        this.prShort = prShort;
    }

    /**
     *
     * @return
     * The prLong
     */
    public String getPrLong() {
        return prLong;
    }

    /**
     *
     * @param prLong
     * The pr_long
     */
    public void setPrLong(String prLong) {
        this.prLong = prLong;
    }
}

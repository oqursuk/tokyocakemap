package com.example.yokura.tokyocakemap.model.restaurant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rest {

    @Expose
    private String id;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @Expose
    private String name;
    @SerializedName("name_kana")
    @Expose
    private String nameKana;
    @Expose
    private String latitude;
    @Expose
    private String longitude;
    @Expose
    private String category;
    @Expose
    private String url;
    @SerializedName("url_mobile")
    @Expose
    private String urlMobile;
    @SerializedName("coupon_url")
    @Expose
    private CouponUrl couponUrl;
    @SerializedName("image_url")
    @Expose
    private ImageUrl imageUrl;
    @Expose
    private String address;
    @Expose
    private String tel;
    @SerializedName("tel_sub")
    @Expose
    private String telSub;

    @Expose
    private String opentime;
    @Expose
    private String holiday;
    @Expose
    private Access access;

    @Expose
    private Pr pr;
    @Expose
    private Code code;
    @Expose
    private String equipment;
    @Expose
    private String budget;
    @Expose
    private String party;
    @Expose
    private String lunch;


    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     *
     * @param updateDate
     * The update_date
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The nameKana
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     *
     * @param nameKana
     * The name_kana
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     *
     * @return
     * The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The urlMobile
     */
    public String getUrlMobile() {
        return urlMobile;
    }

    /**
     *
     * @param urlMobile
     * The url_mobile
     */
    public void setUrlMobile(String urlMobile) {
        this.urlMobile = urlMobile;
    }

    /**
     *
     * @return
     * The couponUrl
     */
    public CouponUrl getCouponUrl() {
        return couponUrl;
    }

    /**
     *
     * @param couponUrl
     * The coupon_url
     */
    public void setCouponUrl(CouponUrl couponUrl) {
        this.couponUrl = couponUrl;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public ImageUrl getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(ImageUrl imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The tel
     */
    public String getTel() {
        return tel;
    }

    /**
     *
     * @param tel
     * The tel
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     *
     * @return
     * The telSub
     */
    public String getTelSub() {
        return telSub;
    }

    /**
     *
     * @param telSub
     * The tel_sub
     */
    public void setTelSub(String telSub) {
        this.telSub = telSub;
    }

    /**
     *
     * @return
     * The opentime
     */
    public String getOpentime() {
        return opentime;
    }

    /**
     *
     * @param opentime
     * The opentime
     */
    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    /**
     *
     * @return
     * The holiday
     */
    public String getHoliday() {
        return holiday;
    }

    /**
     *
     * @param holiday
     * The holiday
     */
    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    /**
     *
     * @return
     * The access
     */
    public Access getAccess() {
        return access;
    }

    /**
     *
     * @param access
     * The access
     */
    public void setAccess(Access access) {
        this.access = access;
    }


    /**
     *
     * @return
     * The pr
     */
    public Pr getPr() {
        return pr;
    }

    /**
     *
     * @param pr
     * The pr
     */
    public void setPr(Pr pr) {
        this.pr = pr;
    }

    /**
     *
     * @return
     * The code
     */
    public Code getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(Code code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The equipment
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     *
     * @param equipment
     * The equipment
     */
    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    /**
     *
     * @return
     * The budget
     */
    public String getBudget() {
        return budget;
    }

    /**
     *
     * @param budget
     * The budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

    /**
     *
     * @return
     * The party
     */
    public String getParty() {
        return party;
    }

    /**
     *
     * @param party
     * The party
     */
    public void setParty(String party) {
        this.party = party;
    }

    /**
     *
     * @return
     * The lunch
     */
    public String getLunch() {
        return lunch;
    }

    /**
     *
     * @param lunch
     * The lunch
     */
    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

}

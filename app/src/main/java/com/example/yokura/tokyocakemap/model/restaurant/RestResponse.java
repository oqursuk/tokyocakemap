package com.example.yokura.tokyocakemap.model.restaurant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by devlop on 15/06/24.
 */
public class RestResponse {
    @SerializedName("total_hit_count")
    @Expose
    private String totalHitCount;
    @SerializedName("hit_per_page")
    @Expose
    private String hitPerPage;
    @SerializedName("page_offset")
    @Expose
    private String pageOffset;
    @Expose
    private List<Rest> rest = new ArrayList<Rest>();

    /**
     *
     * @return
     * The totalHitCount
     */
    public String getTotalHitCount() {
        return totalHitCount;
    }

    /**
     *
     * @param totalHitCount
     * The total_hit_count
     */
    public void setTotalHitCount(String totalHitCount) {
        this.totalHitCount = totalHitCount;
    }

    /**
     *
     * @return
     * The hitPerPage
     */
    public String getHitPerPage() {
        return hitPerPage;
    }

    /**
     *
     * @param hitPerPage
     * The hit_per_page
     */
    public void setHitPerPage(String hitPerPage) {
        this.hitPerPage = hitPerPage;
    }

    /**
     *
     * @return
     * The pageOffset
     */
    public String getPageOffset() {
        return pageOffset;
    }

    /**
     *
     * @param pageOffset
     * The page_offset
     */
    public void setPageOffset(String pageOffset) {
        this.pageOffset = pageOffset;
    }

    /**
     *
     * @return
     * The rest
     */
    public List<Rest> getRest() {
        return rest;
    }

    /**
     *
     * @param rest
     * The rest
     */
    public void setRest(List<Rest> rest) {
        this.rest = rest;
    }
}

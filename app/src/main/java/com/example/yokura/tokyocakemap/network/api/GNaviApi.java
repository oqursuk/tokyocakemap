package com.example.yokura.tokyocakemap.network.api;

import android.location.Location;

import com.example.yokura.tokyocakemap.model.restaurant.Rest;
import com.example.yokura.tokyocakemap.model.typeadapter.RestTypeAdapterFactory;
import com.example.yokura.tokyocakemap.network.service.GNaviService;
import com.example.yokura.tokyocakemap.system.Const;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import rx.Observable;

/**
 * Created by YOKURA on 6/3/15.
 */
public class GNaviApi {
    private static final String TAG = GNaviApi.class.getSimpleName();
    private final GNaviApi self = this;

    public static final String GROUNAVI_DOMAIN = "http://api.gnavi.co.jp";
    public static final String GROUNAVI_APIKEY = "f62676c2315a010802111ea842117cb1";

    public Observable<List<Rest>> getRestList(){

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapterFactory(new RestTypeAdapterFactory())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Const.DOMAIN)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        GNaviService service = restAdapter.create(GNaviService.class);

        Map map = new HashMap<>();
        map.put(Const.PARAM_NAME, "ウオキン");

        return service.restaurantListObs(map);
    }

    public Observable<List<Rest>> getNearByRestList(Location locate){

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapterFactory(new RestTypeAdapterFactory())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Const.DOMAIN)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        GNaviService service = restAdapter.create(GNaviService.class);

        Map param = new HashMap<>();
        param.put(Const.PARAM_LONGITUDE, locate.getLongitude());
        param.put(Const.PARAM_LATITUDE, locate.getLatitude());
        param.put(Const.PARAM_CATEGORY_S, Const.CATEGORY_CAKE);
        param.put(Const.PARAM_RANGE, Const.RANGE_3000M);

        return service.restaurantListObs(param);
    }
}

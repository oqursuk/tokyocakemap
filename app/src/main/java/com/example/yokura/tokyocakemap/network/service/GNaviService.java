package com.example.yokura.tokyocakemap.network.service;

import com.example.yokura.tokyocakemap.model.restaurant.Rest;

import java.util.List;
import java.util.Map;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.QueryMap;
import rx.Observable;

/**
 * Created by YOKURA on 6/3/15.
 */
public interface GNaviService {

//    @GET("/users/{user}/repos")
//    List<String> listRepos(@Path("user") String user);

    @GET("/ver1/RestSearchAPI/?keyid=f62676c2315a010802111ea842117cb1&format=json")
    Observable<Response> restaurantList(@QueryMap Map<String, String> options);

    @GET("/ver1/RestSearchAPI/?keyid=f62676c2315a010802111ea842117cb1&format=json")
    Observable<List<Rest>> restaurantListObs(@QueryMap Map<String, String> options);
    /*
    *
    *
    *
    *
    * */
}

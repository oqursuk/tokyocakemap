package com.example.yokura.tokyocakemap.system;

/**
 * Created by YOKURA on 6/3/15.
 */
public class Const {
    /*
            ぐるなび　レストラン検索API
            http://api.gnavi.co.jp/api/manual/restsearch/
     */


    private static final String TAG = Const.class.getSimpleName();

    private final Const self = this;

    public static String DOMAIN = "http://api.gnavi.co.jp/";

    public static final String CATEGORY_CAKE = "RSFST18005"; //小項目：ケーキカテゴリ

    public static final int RANGE_300M = 1;    //半径3000m
    public static final int RANGE_500M = 2;    //半径3000m
    public static final int RANGE_1000M = 3;    //半径3000m
    public static final int RANGE_2000M = 4;    //半径3000m
    public static final int RANGE_3000M = 5;    //半径3000m

    //リクエストパラメータ
    public static String PARAM_KEYID = "keyid";
    public static String PARAM_ID = "id";
    public static String PARAM_FORMAT = "format";
    public static String PARAM_CALLBACK = "callback";
    public static String PARAM_NAME = "name";
    public static String PARAM_NAME_KANA = "name_kana";
    public static String PARAM_TEL = "tel";
    public static String PARAM_ADDRESS = "address";
    public static String PARAM_AREA = "area";
    public static String PARAM_PREF = "pref";
    public static String PARAM_CATEGORY_L = "category_l";
    public static String PARAM_CATEGORY_S = "category_s";
    public static String PARAM_INPUT_COORDINATES_MODE = "input_coordinates_mode";
    public static String PARAM_EQUIPMENT = "equipment";
    public static String PARAM_COORDINATES_MODE = "coordinates_mode";
    public static String PARAM_LATITUDE = "latitude";
    public static String PARAM_LONGITUDE = "longitude";
    public static String PARAM_RANGE = "range";
    public static String PARAM_SORT = "sort";
    public static String PARAM_OFFSET = "offset";
    public static String PARAM_HIT_PER_PAGE = "hit_per_page";
    public static String PARAM_OFFSET_PAGE = "offset_page";
    public static String PARAM_FREEWORD = "freeword";
    public static String PARAM_FREEWORD_CONDITION = "freeword_condition";
    public static String PARAM_LUNCH = "lunch";
    public static String PARAM_NO_SMOKING = "no_smoking";
    public static String PARAM_CARD = "card";
    public static String PARAM_MOBILEPHONE = "mobilephone";
    public static String PARAM_BOTTOMLESS_CUP = "bottomless_cup";
    public static String PARAM_SUNDAY_OPEN = "sunday_open";
    public static String PARAM_TAKEOUT = "takeout";
    public static String PARAM_PRIVATE_ROOM = "private_room";
    public static String PARAM_MIDNIGHT = "midnight";
    public static String PARAM_PARKING = "parking";
    public static String PARAM_MEMORIAL_SERVICE = "memorial_service";
    public static String PARAM_BIRTHDAY_PRIVILEGE = "birthday_privilege";
    public static String PARAM_BETROTHAL_PRESENT = "betrothal_present";
    public static String PARAM_KIDS_MENU = "kids_menu";
    public static String PARAM_OUTRET = "outret";
    public static String PARAM_WIFI = "wifi";
    public static String PARAM_MICROPHONE = "microphone";
    public static String PARAM_BUFFET = "buffet";
    public static String PARAM_LATE_LUNCH = "late_lunch";
    public static String PARAM_SPORTS = "sports";
    public static String PARAM_UNTIL_MORNING = "until_morning";
    public static String PARAM_LUNCH_DESERT = "lunch_desert";
    public static String PARAM_PROJECTER_SCREEN = "projecter_screen";
    public static String PARAM_WITH_PET = "with_pet";
    public static String PARAM_DELIVERLY = "deliverly";
    public static String PARAM_SPECIAL_HOLIDAY_LUNCH = "special_holiday_lunch";
    public static String PARAM_E_MONEY = "e_money";
    public static String PARAM_CATERLING = "caterling";
    public static String PARAM_BREAKFAST = "breakfast";
    public static String PARAM_DESERT_BUFFET = "desert_buffet";
    public static String PARAM_LUNCH_BUFFET = "lunch_buffet";
    public static String PARAM_BENTO = "bento";
    public static String PARAM_LUNCH_SALAD_BUFFET = "lunch_salad_buffet";
    public static String PARAM_DARTS = "darts";

}

package com.example.yokura.tokyocakemap.ui.activity;

import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.yokura.tokyocakemap.R;
import com.example.yokura.tokyocakemap.model.restaurant.Rest;
import com.example.yokura.tokyocakemap.network.api.GNaviApi;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import rx.android.schedulers.AndroidSchedulers;

public class TopMapActivity extends AppCompatActivity implements SlidingUpPanelLayout.PanelSlideListener {
    private static final String TAG = TopMapActivity.class.getSimpleName();
    @InjectView(R.id.button)
    Button button;
    @InjectView(R.id.mapContainer)
    RelativeLayout mapContainer;
    @InjectView(R.id.transparentView)
    View transparentView;
    @InjectView(R.id.listViewRest)
    ListView listViewRest;

    @InjectView(R.id.slidingContainer)
    RelativeLayout slidingContainer;
    @InjectView(R.id.slidingLayout)
    SlidingUpPanelLayout slidingLayout;

    private SlidingUpPanelLayout mSlidingUpPanelLayout;
    private View mTransparentHeaderView;
    private View mTransparentView;
    private View mSpaceView;
    private MapFragment mMapFragment;
    private GoogleMap mMap;
    private Location mLocation;
    private List<Rest> mRestList = Collections.emptyList();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_map);

        ButterKnife.inject(this);

        getCurrentLocation();

        new GNaviApi().getNearByRestList(mLocation)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    list -> {
                        mRestList = list;
                        setView();
                    }, error ->{
                            Toast.makeText(this,"No result",Toast.LENGTH_LONG);
                            setView();
                            moveToRestLocation(0);
                    });
    }

    private void setView() {

        listViewRest.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        mSlidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.slidingLayout);
        mSlidingUpPanelLayout.setEnableDragViewTouchEvents(true);

        int mapHeight = getResources().getDimensionPixelSize(R.dimen.map_height);
        mSlidingUpPanelLayout.setPanelHeight(mapHeight); // you can use different height here
//        mSlidingUpPanelLayout.setScrollableView(listViewRest, mapHeight);

        mSlidingUpPanelLayout.setPanelSlideListener(this);

        // transparent view at the top of ListView
        mTransparentView = findViewById(R.id.transparentView);

        // init header view for ListView
        mTransparentHeaderView = LayoutInflater.from(this).inflate(R.layout.transparent_header_view, null, false);
        mSpaceView = mTransparentHeaderView.findViewById(R.id.space);

        listViewRest.addHeaderView(mTransparentHeaderView);
        listViewRest.setAdapter(new ArrayAdapter<Rest>(this, R.layout.simple_list_item, mRestList));

        collapseMap();

        mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mapContainer, mMapFragment, "map");
        fragmentTransaction.commit();
        setUpMapIfNeeded();
    }


    private void getCurrentLocation() {
        //LocationManagerの取得
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //GPSから現在地の情報を取得
        mLocation = locationManager.getLastKnownLocation("gps");
    }

    @OnItemClick(R.id.listViewRest)
    public void moveToRestLocation(int position) {
        Rest rest = mRestList.get(position);
        LatLng coordinate;

        if( rest == null) {
            coordinate = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        } else {
            coordinate = new LatLng(Double.valueOf(rest.getLatitude()), Double.valueOf(rest.getLongitude()));
        }

        MarkerOptions options = new MarkerOptions();
        options.position(coordinate);
        options.title(rest.getName());

        mMap.addMarker(options);

        CameraPosition destination = new CameraPosition.Builder()
                .target(coordinate).zoom(15.5f)
                .bearing(0).tilt(25).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(destination));
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = mMapFragment.getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setCompassEnabled(false);
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                CameraUpdate update = getLastKnownLocation();
                if (update != null) {
                    mMap.moveCamera(update);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // In case Google Play services has since become available.
        //setUpMapIfNeeded();
    }

    private CameraUpdate getLastKnownLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        String provider = lm.getBestProvider(criteria, true);
        if (provider == null) {
            return null;
        }
        Location loc = lm.getLastKnownLocation(provider);

        if (loc != null) {
            return CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 14.0f));
        }
        return null;
    }

    private void collapseMap() {
        mSpaceView.setVisibility(View.VISIBLE);
        mTransparentView.setVisibility(View.GONE);
    }

    private void expandMap() {
        mSpaceView.setVisibility(View.GONE);
        mTransparentView.setVisibility(View.INVISIBLE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_top_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPanelSlide(View view, float v) {

    }

    @Override
    public void onPanelCollapsed(View view) {

    }

    @Override
    public void onPanelExpanded(View view) {

    }

    @Override
    public void onPanelAnchored(View view) {

    }

    @Override
    public void onPanelHidden(View view) {

    }

}

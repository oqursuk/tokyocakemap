package com.example.yokura.tokyocakemap.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yokura.tokyocakemap.R;
import com.example.yokura.tokyocakemap.model.restaurant.Rest;
import com.example.yokura.tokyocakemap.ui.view.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by devlop on 15/06/24.
 */
public class RestAdapter extends ArrayAdapter<Rest> {
    private LayoutInflater mLayoutInflater;

    public RestAdapter(Context context, int resource, List<Rest> restList) {
        super(context, resource, restList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Rest rest = getItem(position);

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_list_rest, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textRestName.setText(rest.getName());
        Picasso.with(getContext()).load(rest.getImageUrl().getShopImage1())
                .fit().transform(new CircleTransform()).into(holder.shopImage);

        return super.getView(position, convertView, parent);
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'row_list_rest.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class ViewHolder {
        @InjectView(R.id.shopImage)
        ImageView shopImage;
        @InjectView(R.id.textRestName)
        TextView textRestName;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
